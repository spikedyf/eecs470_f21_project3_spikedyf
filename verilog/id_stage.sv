/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  id_stage.v                                          //
//                                                                     //
//  Description :  instruction decode (ID) stage of the pipeline;      //
//                 decode the instruction fetch register operands, and //
//                 compute immediate operand (if applicable)           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


`timescale 1ns/100ps


// Decode an instruction: given instruction bits IR produce the
// appropriate datapath control signals.
//
// This is a *combinational* module (basically a PLA).
//
module decoder (
  //input [31:0] inst,
  //input valid_inst_in,  // ignore inst when low, outputs will
                                    // reflect noop (except valid_inst)
  //see sys_defs.svh for definition
  input  IF_ID_PACKET   if_packet ,
  output ALU_OPA_SELECT opa_select,
  output ALU_OPB_SELECT opb_select,
  output DEST_REG_SEL   dest_reg  , // mux selects
  output ALU_FUNC       alu_func  ,
  output logic          rd_mem, wr_mem, cond_branch, uncond_branch,
  output logic          csr_op    , // used for CSR operations, we only used this as
                                    //a cheap way to get the return code out
  output logic          halt      , // non-zero on a halt
  output logic          illegal   , // non-zero on an illegal instruction
  output logic          valid_inst, // for counting valid instructions executed
                                    // and for making the fetch stage die on halts/
                                    // keeping track of when to allow the next
                                    // instruction out of fetch
                                    // 0 for HALT and illegal instructions (die on halt)
  output logic          stall       //出现data hazards
);

  INST  inst         ;
  logic valid_inst_in;

  assign inst          = if_packet.inst;
  assign valid_inst_in = if_packet.valid;
  assign valid_inst    = valid_inst_in & ~illegal;

  always_comb begin
    // default control values:
    // - valid instructions must override these defaults as necessary.
    //	 opa_select, opb_select, and alu_func should be set explicitly.
    // - invalid instructions should clear valid_inst.
    // - These defaults are equivalent to a noop
    // * see sys_defs.vh for the constants used here
    opa_select    = OPA_IS_RS1;
    opb_select    = OPB_IS_RS2;
    alu_func      = ALU_ADD;
    dest_reg      = DEST_NONE;
    csr_op        = `FALSE;
    rd_mem        = `FALSE;
    wr_mem        = `FALSE;
    cond_branch   = `FALSE;
    uncond_branch = `FALSE;
    halt          = `FALSE;
    illegal       = `FALSE;
    if(valid_inst_in) begin
      casez (inst)
        `RV32_LUI : begin
          dest_reg   = DEST_RD;
          opa_select = OPA_IS_ZERO;
          opb_select = OPB_IS_U_IMM;
        end
        `RV32_AUIPC : begin
          dest_reg   = DEST_RD;
          opa_select = OPA_IS_PC;
          opb_select = OPB_IS_U_IMM;
        end
        `RV32_JAL : begin
          dest_reg      = DEST_RD;
          opa_select    = OPA_IS_PC;
          opb_select    = OPB_IS_J_IMM;
          uncond_branch = `TRUE;
        end
        `RV32_JALR : begin
          dest_reg      = DEST_RD;
          opa_select    = OPA_IS_RS1;
          opb_select    = OPB_IS_I_IMM;
          uncond_branch = `TRUE;
        end
        `RV32_BEQ, `RV32_BNE, `RV32_BLT, `RV32_BGE,
        `RV32_BLTU, `RV32_BGEU: begin
          opa_select  = OPA_IS_PC;
          opb_select  = OPB_IS_B_IMM;
          cond_branch = `TRUE;
        end
        `RV32_LB, `RV32_LH, `RV32_LW,
        `RV32_LBU, `RV32_LHU: begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          rd_mem     = `TRUE;
        end
        `RV32_SB, `RV32_SH, `RV32_SW: begin
          opb_select = OPB_IS_S_IMM;
          wr_mem     = `TRUE;
        end
        `RV32_ADDI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
        end
        `RV32_SLTI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_SLT;
        end
        `RV32_SLTIU : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_SLTU;
        end
        `RV32_ANDI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_AND;
        end
        `RV32_ORI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_OR;
        end
        `RV32_XORI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_XOR;
        end
        `RV32_SLLI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_SLL;
        end
        `RV32_SRLI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_SRL;
        end
        `RV32_SRAI : begin
          dest_reg   = DEST_RD;
          opb_select = OPB_IS_I_IMM;
          alu_func   = ALU_SRA;
        end
        `RV32_ADD : begin
          dest_reg = DEST_RD;
        end
        `RV32_SUB : begin
          dest_reg = DEST_RD;
          alu_func = ALU_SUB;
        end
        `RV32_SLT : begin
          dest_reg = DEST_RD;
          alu_func = ALU_SLT;
        end
        `RV32_SLTU : begin
          dest_reg = DEST_RD;
          alu_func = ALU_SLTU;
        end
        `RV32_AND : begin
          dest_reg = DEST_RD;
          alu_func = ALU_AND;
        end
        `RV32_OR : begin
          dest_reg = DEST_RD;
          alu_func = ALU_OR;
        end
        `RV32_XOR : begin
          dest_reg = DEST_RD;
          alu_func = ALU_XOR;
        end
        `RV32_SLL : begin
          dest_reg = DEST_RD;
          alu_func = ALU_SLL;
        end
        `RV32_SRL : begin
          dest_reg = DEST_RD;
          alu_func = ALU_SRL;
        end
        `RV32_SRA : begin
          dest_reg = DEST_RD;
          alu_func = ALU_SRA;
        end
        `RV32_MUL : begin
          dest_reg = DEST_RD;
          alu_func = ALU_MUL;
        end
        `RV32_MULH : begin
          dest_reg = DEST_RD;
          alu_func = ALU_MULH;
        end
        `RV32_MULHSU : begin
          dest_reg = DEST_RD;
          alu_func = ALU_MULHSU;
        end
        `RV32_MULHU : begin
          dest_reg = DEST_RD;
          alu_func = ALU_MULHU;
        end
        `RV32_CSRRW, `RV32_CSRRS, `RV32_CSRRC: begin
          csr_op = `TRUE;
        end
        `WFI : begin
          halt = `TRUE;
        end
        default : illegal = `TRUE;

      endcase // casez (inst)
    end // if(valid_inst_in)
  end // always
endmodule // decoder


module id_stage (
  input                                clock             , // system clock
  input                                reset             , // system reset
  input                                wb_reg_wr_en_out  , // Reg write enable from WB Stage
  input                    [      4:0] wb_reg_wr_idx_out , // Reg write index from WB Stage
  input                    [`XLEN-1:0] wb_reg_wr_data_out, // Reg write data from WB Stage
  input  IF_ID_PACKET                  if_id_packet_in   ,
  input                                squash            , //是否要take branch
  // input                                structural_hazard ,
  output ID_EX_PACKET                  id_packet_out     ,
  output                               stall             , //判断要不要stall,
  output RS_SELECTOR                   select_result     ,
  output COMPARATOR_PACKET [      2:0] out_store
);
  ID_EX_PACKET tmp_packet;
  assign tmp_packet.inst = if_id_packet_in.inst;
  assign tmp_packet.NPC  = if_id_packet_in.NPC;
  assign tmp_packet.PC   = if_id_packet_in.PC;
  DEST_REG_SEL dest_reg_select;

  // Instantiate the register file used by this pipeline
  regfile regf_0 (
    .rda_idx(if_id_packet_in.inst.r.rs1),
    .rda_out(tmp_packet.rs1_value      ),
    
    .rdb_idx(if_id_packet_in.inst.r.rs2),
    .rdb_out(tmp_packet.rs2_value      ),
    
    .wr_clk (clock                     ),
    .wr_en  (wb_reg_wr_en_out          ),
    .wr_idx (wb_reg_wr_idx_out         ),
    .wr_data(wb_reg_wr_data_out        )
  );

  // instantiate the instruction decoder
  decoder decoder_0 (
    .if_packet    (if_id_packet_in         ),
    // Outputs
    .opa_select   (tmp_packet.opa_select   ),
    .opb_select   (tmp_packet.opb_select   ),
    .alu_func     (tmp_packet.alu_func     ),
    .dest_reg     (dest_reg_select         ),
    .rd_mem       (tmp_packet.rd_mem       ),
    .wr_mem       (tmp_packet.wr_mem       ),
    .cond_branch  (tmp_packet.cond_branch  ),
    .uncond_branch(tmp_packet.uncond_branch),
    .csr_op       (tmp_packet.csr_op       ),
    .halt         (tmp_packet.halt         ),
    .illegal      (tmp_packet.illegal      ),
    .valid_inst   (tmp_packet.valid        )
  );

  // mux to generate dest_reg_idx based on
  // the dest_reg_select output from decoder
  always_comb begin
    case (dest_reg_select)
      DEST_RD   : tmp_packet.dest_reg_idx = if_id_packet_in.inst.r.rd;
      DEST_NONE : tmp_packet.dest_reg_idx = `ZERO_REG;
      default   : tmp_packet.dest_reg_idx = `ZERO_REG;
    endcase
  end

  comparator cmp1 (
    .clock        (clock                     ),
    .reset        (reset                     ),
    .squash       (squash                    ),
    .dest_reg     (tmp_packet.dest_reg_idx   ),
    .rs1_reg      (if_id_packet_in.inst.r.rs1),
    .rs2_reg      (if_id_packet_in.inst.r.rs2),
    .rs1_select   (tmp_packet.opa_select     ),
    .rs2_select   (tmp_packet.opb_select     ),
    .input_packet (tmp_packet                ),
    .inst_valid   (tmp_packet.valid          ),
    // .structural_hazard(structural_hazard         ),
    .stall        (stall                     ),
    .output_packet(id_packet_out             ),
    .select_result(select_result             ),
    .out_store    (out_store                 )
  );






endmodule // module id_stage

module comparator (
  input                          clock        ,
  input                          reset        ,
  input                          squash       ,
  input                    [4:0] dest_reg     ,
  input                    [4:0] rs1_reg      ,
  input                    [4:0] rs2_reg      ,
  input  ALU_OPA_SELECT          rs1_select   ,
  input  ALU_OPB_SELECT          rs2_select   ,
  input  ID_EX_PACKET            input_packet ,
  input                          inst_valid   ,
  // input                          structural_hazard,
  output logic                   stall        ,
  output ID_EX_PACKET            output_packet,
  output RS_SELECTOR             select_result,
  output COMPARATOR_PACKET [2:0] out_store
);
  COMPARATOR_PACKET[2:0] reg_store;
  assign out_store = reg_store;
  logic stall_a, stall_b;

  assign stall = stall_a | stall_b;
  always_comb
    begin
      output_packet = input_packet;
      if(inst_valid & reg_store[0].valid & reg_store[0].dest_reg != 5'h0 & rs1_reg == reg_store[0].dest_reg & (rs1_select == OPA_IS_RS1 | input_packet.cond_branch == `TRUE) & reg_store[0].is_load)
        begin
          stall_a = 1'b1;
        end
      else
        begin
          stall_a = 1'b0;
          if(inst_valid & reg_store[0].valid & reg_store[0].dest_reg != 5'h0 & rs1_reg == reg_store[0].dest_reg)
            begin
              select_result.rs1_is_one_forward = 1'b1;
              select_result.rs1_is_two_forward = 1'b0;
            end
          else if(inst_valid & reg_store[1].valid & reg_store[1].dest_reg != 5'h0 & rs1_reg == reg_store[1].dest_reg)
            begin
              select_result.rs1_is_one_forward = 1'b0;
              select_result.rs1_is_two_forward = 1'b1;
            end
          else
            begin
              select_result.rs1_is_one_forward = 1'b0;
              select_result.rs1_is_two_forward = 1'b0;
            end
        end


      if(inst_valid & reg_store[0].valid & reg_store[0].dest_reg != 5'h0 & rs2_reg == reg_store[0].dest_reg & (rs2_select == OPB_IS_RS2 | input_packet.cond_branch == `TRUE | rs2_select == OPB_IS_S_IMM) & reg_store[0].is_load)
        begin
          stall_b = 1'b1;
        end
      else
        begin
          stall_b = 1'b0;
          if(inst_valid & reg_store[0].valid & reg_store[0].dest_reg != 5'h0 & rs2_reg == reg_store[0].dest_reg)
            begin
              select_result.rs2_is_one_forward = 1'b1;
              select_result.rs2_is_two_forward = 1'b0;
            end
          else if(inst_valid & reg_store[1].valid & reg_store[1].dest_reg != 5'h0 & rs2_reg == reg_store[1].dest_reg)
            begin
              select_result.rs2_is_one_forward = 1'b0;
              select_result.rs2_is_two_forward = 1'b1;
            end
          else
            begin
              select_result.rs2_is_one_forward = 1'b0;
              select_result.rs2_is_two_forward = 1'b0;
            end
        end
      if(stall_a | stall_b | squash)
        begin
          output_packet.opa_select    = OPA_IS_RS1;
          output_packet.opb_select    = OPB_IS_RS2;
          output_packet.inst          = `NOP;
          output_packet.dest_reg_idx  = `ZERO_REG;
          output_packet.alu_func      = ALU_ADD;
          output_packet.csr_op        = `FALSE;
          output_packet.rd_mem        = `FALSE;
          output_packet.wr_mem        = `FALSE;
          output_packet.cond_branch   = `FALSE;
          output_packet.uncond_branch = `FALSE;
          output_packet.halt          = `FALSE;
          output_packet.illegal       = `FALSE;
          output_packet.valid         = `FALSE;
        end
    end

  always_ff @(posedge clock)
    begin
      if(reset)
        begin
          reg_store[0].valid <= `SD 1'b0;
          reg_store[1].valid <= `SD 1'b0;
          reg_store[2].valid <= `SD 1'b0;
          reg_store[0].dest_reg <= `SD 5'b0;
          reg_store[1].dest_reg <= `SD 5'b0;
          reg_store[2].dest_reg <= `SD 5'b0;
          reg_store[0].is_load <= `SD 1'b0;
          reg_store[1].is_load <= `SD 1'b0;
          reg_store[2].is_load <= `SD 1'b0;
        end
      else if(squash)
        begin
          reg_store[2] <= `SD reg_store[1];
          reg_store[1].valid <= `SD 1'b0;
          reg_store[1].dest_reg <= `SD 5'b0;
          reg_store[1].is_load <= `SD 1'b0;
          reg_store[0].valid <= `SD 1'b0;
          reg_store[0].dest_reg <= `SD 5'b0;
          reg_store[0].is_load <= `SD 1'b0;
        end
      else if(stall)
        begin
          reg_store[2] <= `SD reg_store[1];
          reg_store[1] <= `SD reg_store[0];
          reg_store[0].valid <= `SD 1'b0;
          reg_store[0].dest_reg <= `SD 5'b0;
          reg_store[0].is_load <= `SD 1'b0;
        end
      else
        begin
          reg_store[2] <= `SD reg_store[1];
          reg_store[1] <= `SD reg_store[0];
          reg_store[0].dest_reg <= `SD dest_reg;
          reg_store[0].valid <= `SD inst_valid;
          reg_store[0].is_load <= `SD output_packet.rd_mem;
        end

    end



endmodule