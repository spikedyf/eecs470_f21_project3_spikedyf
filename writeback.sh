#!/usr/bin/bash
#
# This file will allow automated checking of project 3. It requires that you create a git branch with
# the unmodified project 3 starter code (BASEBRANCH), a branch with your modified code (TESTBRANCH) 
# and a directory holding test programs (TESTCASES)
#

export BASEOUTDIR=base_outputs
export TESTOUTDIR=test_outputs
export TESTCASES=test_progs
export BASEBRANCH=base
export TESTBRANCH=master
export ASSEMBLY_EXT=s
export PROGRAM_EXT=c

function generate_base_outputs {
    if ! [ -d $BASEOUTDIR ]; then
        echo "$0: Making base outputs..."
        mkdir $BASEOUTDIR
        branch_temp=`git rev-parse --abbrev-ref HEAD`
        git checkout $BASEBRANCH
        echo "$0: Building base simv on branch [$BASEBRANCH]"
        make clean syn_simv > /dev/null
        echo "$0: Done."
        run_tests ${ASSEMBLY_EXT} ${BASEOUTDIR} assembly
        run_tests ${PROGRAM_EXT} ${BASEOUTDIR} program
        git checkout $branch_temp
    fi
}

function run_tests {
    extension=$1
    output=$2
    type=$3
    echo "$0: Testing $type files"
    for tst in $TESTCASES/*.$extension; do
        testname=$tst
        testname=${testname##${TESTCASES}\/}
        testname=${testname%%.${extension}}
        echo "$0: Test: $testname"
        make $type SOURCE=$tst > /dev/null
        ./syn_simv > program.out
        grep "@@@" program.out > $2/$testname.program.out
        grep "CPI" program.out > $2/$testname.cpi.out
        mv pipeline.out $2/$testname.pipeline.out
        mv writeback.out $2/$testname.writeback.out
    done
}

function generate_test_outputs {
    # create test outputs
    git checkout $TESTBRANCH
    if [ -d $TESTOUTDIR ]; then
        echo "$0: Deleting old test outputs from $TESTOUTDIR"
        rm -r $TESTOUTDIR/*
    else
        mkdir $TESTOUTDIR
    fi
    echo "$0: Building Test simv on branch [$TESTBRANCH]"
    make clean syn_simv > /dev/null
    echo "$0: Done."
    run_tests ${ASSEMBLY_EXT} ${TESTOUTDIR} assembly
    run_tests ${PROGRAM_EXT} ${TESTOUTDIR} program
}

function compare_results {
    printf "\TEST RESULTS:\n"
    # compare results
    pass_count=$((0))
    fail_count=$((0))
    total=$((0))
    for tst in $TESTOUTDIR/*.program.out; do
        testname=$tst
        testname=${testname##${TESTOUTDIR}\/}
        testname=${testname%%.program.out}
        diff $tst $BASEOUTDIR/$testname.program.out > /dev/null
        status=$? # 0 -> no difference
        if [[ "$status" -eq "0" ]]; then
            echo "$0: Test $testname PASSED"
            pass_count=$(($pass_count + 1))
        else
            echo "$0: Test $testname FAILED"
            fail_count=$(($fail_count + 1))
        fi
        echo "BASE PERF `cat $BASEOUTDIR/$testname.cpi.out`"
        echo "TEST PERF `cat $TESTOUTDIR/$testname.cpi.out`"
        echo ""
        total=$(($total + 1))
    done
    for tst in $TESTOUTDIR/*.writeback.out; do
        testname=$tst
        testname=${testname##${TESTOUTDIR}\/}
        testname=${testname%%.writeback.out}
        diff $tst $BASEOUTDIR/$testname.writeback.out > /dev/null
        status=$? # 0 -> no difference
        if [[ "$status" -eq "0" ]]; then
            echo "$0: Test $testname PASSED"
            pass_count=$(($pass_count + 1))
        else
            echo "$0: Test $testname FAILED"
            fail_count=$(($fail_count + 1))
        fi
        echo "BASE PERF `cat $BASEOUTDIR/$testname.cpi.out`"
        echo "TEST PERF `cat $TESTOUTDIR/$testname.cpi.out`"
        echo ""
        total=$(($total + 1))
    done
    echo ""
    echo "PASSED $pass_count/$total tests ($fail_count failures)."
}



compare_results

